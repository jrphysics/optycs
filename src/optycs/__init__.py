# -*- coding: utf-8 -*-
"""
The tools you need to solve your optics problems.

At the moment the included tools are focused in optical design.
In particular, the main interest here lays in the pre-design
(neglected by most optical design products).
"""


from pkg_resources import get_distribution, DistributionNotFound

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    __version__ = 'unknown'
finally:
    del get_distribution, DistributionNotFound
