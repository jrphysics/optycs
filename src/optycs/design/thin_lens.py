"""
Set of tools to help with thin lens pre-design.

Includes:
    |  Paraxial calculations.
    |  Paraxial ray tracing equations.
    |  Aberration Seidel coefficients.
    |  Stop shift equations for Seidels.
"""
import numpy as np
import warnings


# LENS DESIGN SYMBOLS
def K(R1, R2, n_lens, n_medium=1):
    """Power, same as function 'power'."""
    return power(R1, R2, n_lens, n_medium)


def f(R1, R2, n_lens, n_medium=1):
    """Focal length, same as function 'focal_length'."""
    return focal_length(R1, R2, n_lens, n_medium)


def B(R1, R2):
    """Shape factor, same as function 'shape_factor'."""
    return shape_factor(R1, R2)


def H(h_marginal, u_chief, n=1):
    """Optical invariant, same as function 'optical_invariant'. At Stop."""
    return optical_invariant(h_marginal, u_chief, n)


def BFL(y, u):
    """Back focal length, same as function 'back_focal_length'."""
    return back_focal_length(y, u)


# Seidels
def S1(h, k, b, g, n):
    """Seidel coefficient 1 (Spherical aberration), same as 'seidel1_stop'."""
    return seidel1_stop(h, k, b, g, n)


def S2(h, k, b, g, n, h_inv):
    """Seidel coefficient 2 (Coma), same as 'seidel2_stop'."""
    return seidel2_stop(h, k, b, g, n, h_inv)


def S3(k, h_inv):
    """Seidel coefficient 3 (Astigmatism), same as 'seidel3_stop'."""
    return seidel3_stop(k, h_inv)


def S4(k, n, h_inv):
    """Seidel coefficient 4 (Field curvature), same as 'seidel4_stop'."""
    return seidel4_stop(k, n, h_inv)


def S5():
    """Seidel coefficient 5 (Distortion), same as 'seidel5_stop'."""
    return seidel5_stop()


def C1(h, k, v):
    """Axial chromatic coefficient, same as 'chromatic1_stop'."""
    return chromatic1_stop(h, k, v)


def C2():
    """Transverse chromatic coefficient, same as 'chromatic2_stop'."""
    return chromatic2_stop()


def S1s(s1):
    """Stop shift equation for 1st Seidel coefficient, same as 'seidel1_shift'.
    """
    return seidel1_shift(s1)


def S2s(s1, s2, h_marginal, h_chief):
    """Stop shift equation for 2nd Seidel coefficient, same as 'seidel2_shift'.
    """
    return seidel2_shift(s1, s2, h_marginal, h_chief)


def S3s(s1, s2, s3, h_marginal, h_chief):
    """Stop shift equation for 3rd Seidel coefficient, same as 'seidel3_shift'.
    """
    return seidel3_shift(s1, s2, s3, h_marginal, h_chief)


def S4s(s4):
    """Stop shift equation for 4th Seidel coefficient, same as 'seidel4_shift'.
    """
    return seidel4_shift(s4)


def S5s(s1, s2, s3, s4, s5, h_marginal, h_chief):
    """Stop shift equation for 5th Seidel coefficient, same as 'seidel5_shift'.
    """
    return seidel5_shift(s1, s2, s3, s4, s5, h_marginal, h_chief)


def C1s(c1):
    """
    Stop shift eq. for 1st chromatic coefficient, same as 'chromatic1_shift'.
    """
    return chromatic1_shift(c1)


def C2s(c1, c2, h_marginal, h_chief):
    """
    Stop shift eq. for 2nd chromatic coefficient, same as 'chromatic2_shift'.
    """
    return chromatic2_shift(c1, c2, h_marginal, h_chief)


# END LENS DESIGN SYMBOLS


# EXPLICITLY NAMED FUNCTIONS
def power(r1, r2, n_lens, n_medium=1):
    """
    Calculate thin lens power based on its radii.

    Parameters
    ----------
    r1: float
        Front surface radius.
    r2: float
        Back surface radius.
    n_lens: float
        Refractive index between surfaces.
    n_medium: float, default 1
        Refractive index of surrounding medium.

    Returns
    -------
    k: float
        Thin lens power.
    """

    # Calculate curvatures for convenience
    curvature_1 = 1 / r1
    curvature_2 = 1 / r2
    n_factor = (n_lens / n_medium) - 1
    k = n_factor * (curvature_1 - curvature_2)
    return k


def focal_length(r1, r2, n_lens, n_medium=1):
    """
    Calculate thin lens focal length. This is the inverse of the power.

    Parameters
    ----------
    r1: float
        Front surface radius.
    r2: float
        Back surface radius.
    n_lens: float
        Refractive index between surfaces.
    n_medium: float, default 1
        Refractive index of surrounding medium.

    Returns
    -------
    f: float
        Thin Lens focal length.
    """

    k = power(r1, r2, n_lens, n_medium)
    f_length = 1 / k
    return f_length


def power_semi_thick(k1, k2, t, n_lens):
    """
    Lens power for (unreal) thin lens with thickness.

    Parameters
    ----------
    k1: float
        Front surface power.
    k2: float
        Back surface power.
    t: float
        Thickness between both surfaces.
    n_lens: float
        Refractive index between both surfaces.

    Returns
    -------
    k_semi_thick: float
        Semi-thick lens power.
    """

    k_semi_thick = k1 + k2 - t / n_lens * k1 * k2
    return k_semi_thick


def shape_factor(r1, r2):
    """
    Coddington lens shape.

    Maybe more convenient alternative is using curvatures, given:
    b = (r2 + r1) / (r2 - r1) # This can explode for flat surfaces.

    Parameters
    ----------
    r1: float
        Lens front radius.
    r2: float
        Lens back radius.

    Returns
    -------
    b: float
        Lens shape factor.
    """

    curvature_1 = 1 / r1
    curvature_2 = 1 / r2
    if curvature_1 == curvature_2:
        b = 0
    else:
        b = (curvature_1 + curvature_2) / (curvature_1 - curvature_2)
    return b


def position_factor_u(u, u_prim):
    """
    Coddington position factor in terms of object and image angles.
    Image side: '_prim'.

    Parameters
    ----------
    u: float
        Input ray angle.
    u_prim: float
        Output ray angle.

    Returns
    -------
    g: float
        Coddington shape factor.

    """
    g = (u_prim + u) / (-u_prim + u)
    return g


def position_factor_s(so, si):
    """
    Coddington position factor in terms of distances (o: object. i: image).

    Parameters
    ----------
    so: float
        Object distance.
    si: float
        Image distance.

    Returns
    -------
    g: float
        Coddington shape factor.
    """


    if so * si == 0:
        raise ValueError("Object/Image cannot be in the lens.")
    elif so == np.inf:
        g = -1
    elif si == np.inf:
        g = 1
    else:
        g = (si + so) / (si - so)
    return g


def position_factor_mag(m):
    """
    Coddington position factor in terms of magnification.

    Parameters
    ----------
    m: float
        Lens magnification.

    Returns
    -------
    g: float
        Coddington shape factor.

    """
    if m == 1:
        g = -1
    else:
        g = (m + 1) / (m - 1)
    return g


def radii_from_shape(k, b, n_lens, n_medium=1):
    """
    Obtain lens radii from lens power and shape.

    Parameters
    ----------
    k: float
        Lens power.
    b: float
        Lens shape factor.
    n_lens: float
        Lens refractive index.
    n_medium: float, default 1
        Medium refractive index.

    Returns
    -------
    r1: float
        Lens front radius.
    r2: float
        Lens back radius.
    """
    n_factor = (n_lens / n_medium) - 1

    # Calculate curvatures
    curvature_1 = 1 / 2 * ((b + 1) * k / n_factor)
    curvature_2 = 1 / 2 * ((b - 1) * k / n_factor)

    # Transform to radii
    r1 = np.divide(1, curvature_1)
    r2 = np.divide(1, curvature_2)

    return r1, r2


# Optical invariant
def optical_invariant(h_margin, u_chief, h_chief=0, u_margin=0, n_medium=1):
    """
    Calculate optical invariant. Default at STOP so h_chief·u_marginal = 0.

    Parameters
    ----------
    h_margin: float
        Marginal ray height.
    u_chief: float
        Chief ray angle.
    h_chief: float, default 0
        Chief ray height.
    u_margin: float, default 0
        Marginal ray angle.
    n_medium: float, default 1
        Refractive index of medium (not lens).

    Returns
    -------
    h_inv: float
        Optical invariant.
    """

    h_inv = n_medium * (h_chief * u_margin - h_margin * u_chief)
    return h_inv


# SEIDEL COEFFICIENTS
def seidel1_stop(h, k, b, g, n):
    """
    Seidel coefficient for Spherical. Lens at Stop.

    Parameters
    ----------
    h: float
        Marginal ray height.
    k: float
        Element power.
    b: float
        Shape factor.
    g: float
        Position factor.
    n: float
        Refractive index.

    Returns
    -------
    s1: float
        First Seidel coefficient.
    """

    # Refractive index coefficients
    mu1, mu2, mu3, mu4, *__ = coefficients_refractive_index(n)
    # Summands
    a1 = mu1 * b ** 2
    a2 = mu2 * b * g
    a3 = mu3 * g ** 2
    a4 = mu4

    s1 = h ** 4 * k ** 3 / 4 * (a1 + a2 + a3 + a4)
    return s1


def seidel2_stop(h, k, b, g, n, h_inv):
    """
    Seidel coefficient for Coma. Lens at Stop.

    Parameters
    ----------
    h: float
        Marginal ray height.
    k: float
        Element power.
    b: float
        Shape factor.
    g: float
        Position factor.
    n: float
        Refractive index.
    h_inv: float
        Optical invariant.

    Returns
    -------
    s2: float
        Second Seidel coefficient.
    """

    # Refractive index coefficients
    *__, mu5, mu6 = coefficients_refractive_index(n)
    # Summands
    a1 = mu5 * b
    a2 = mu6 * g

    s2 = h ** 2 * k ** 2 * h_inv / 2 * (a1 + a2)  # Not sure about the sign
    return s2


def seidel3_stop(k, h_inv):
    """
    Seidel coefficient for Astigmatism. Lens at Stop.

    Parameters
    ----------
    k: float
        Element power.
    h_inv: float
        Optical invariant.

    Returns
    -------
    s3: float
        Third Seidel coefficient.
    """

    s3 = h_inv ** 2 * k
    return s3


def seidel4_stop(k, n, h_inv):
    """
    Seidel coefficient for Field Curvature. Lens at Stop.

    Parameters
    ----------
    k: float
        Element power.
    n: float
        Refractive index.
    h_inv: float
        Optical invariant.

    Returns
    -------
    S4: float
        Fourth Seidel coefficient.
    """

    s4 = h_inv ** 2 * k / n
    return s4


def seidel5_stop():
    """Seidel coefficient for Distortion. Lens at Stop. This is always 0."""
    s5 = 0
    return s5


def chromatic1_stop(h, k, v):
    """
    Seidel coefficient for Axial Chromatic. Lens at Stop.

    Parameters
    ----------
    h: float
        Marginal ray height.
    k: float
        Element power.
    v: float
        Abbe number.

    Returns
    -------
    c1: float
        First Chromatic coefficient.
    """

    c1 = h ** 2 * k / v
    return c1


def chromatic2_stop():
    """Seidel coefficient for Lateral Chromatic.
    Lens at Stop. This coefficient is always 0.
    """
    c2 = 0
    return c2


def coefficients_refractive_index(n):
    """
    Refractive index coefficients for Seidel equations.

    Parameters
    ----------
    n: float
        Refractive index.

    Returns
    -------
    mu1, ..., mu6: float
        Refractive index coefficients for Seidel equations.
    """

    mu1 = (n + 2) / (n*(n - 1)**2)
    mu2 = (4*n + 4) / (n*(n - 1))
    mu3 = (3*n + 2) / n
    mu4 = n**2 / (n - 1)**2
    mu5 = (n + 1) / (n*(n - 1))
    mu6 = (2*n + 1) / n

    return mu1, mu2, mu3, mu4, mu5, mu6


# STOP SHIFT EQUATIONS
def seidel1_shift(s1):
    """
    Stop shift equation for first Seidel coefficient.

    Parameters
    ----------
    s1: float
        First Seidel coefficient for lens at Stop.

    Returns
    -------
    s1s: float
        First Seidel coefficient for lens away from Stop.
    """
    s1s = s1
    return s1s


def seidel2_shift(s1, s2, h_marginal, h_chief):
    """
    Stop shift equation for second Seidel coefficient.

    Parameters
    ----------
    s1: float
        First Seidel coefficient for lens at Stop.
    s2: float
        Second Seidel coefficient for lens at Stop.
    h_marginal: float
        Marginal ray height at lens.
    h_chief: float
        Chief ray height at lens.

    Returns
    -------
    s2s: float
        Second Seidel coefficient for lens away from Stop.
    """
    q = h_chief / h_marginal
    s2s = s2 + q * s1
    return s2s


def seidel3_shift(s1, s2, s3, h_marginal, h_chief):
    """
    Stop shift equation for third Seidel coefficient.

    Parameters
    ----------
    s1: float
        First Seidel coefficient for lens at Stop.
    s2: float
        Second Seidel coefficient for lens at Stop.
    s3: float
        Third Seidel coefficient for lens at Stop.
    h_marginal: float
        Marginal ray height at lens.
    h_chief: float
        Chief ray height at lens.

    Returns
    -------
    s3s: float
        Third Seidel coefficient for lens away from Stop.
    """
    q = h_chief / h_marginal
    s3s = s3 + 2 * q * s2 + q ** 2 * s1
    return s3s


def seidel4_shift(s4):
    """
    Stop shift equation for fourth Seidel coefficient.

    Parameters
    ----------
    s4: float
        Fourth Seidel coefficient for lens at Stop.

    Returns
    -------
    s4s: float
        Fourth Seidel coefficient for lens away from Stop.
    """
    s4s = s4
    return s4s


def seidel5_shift(s1, s2, s3, s4, s5, h_marginal, h_chief):
    """
    Stop shift equation for fifth Seidel coefficient.

    Parameters
    ----------
    s1: float
        First Seidel coefficient for lens at Stop.
    s2: float
        Second Seidel coefficient for lens at Stop.
    s3: float
        Third Seidel coefficient for lens at Stop.
    s4: float
        Fourth Seidel coefficient for lens at Stop.
    s5: float
        Fifth Seidel coefficient for lens at Stop.
    h_marginal: float
        Marginal ray height at lens.
    h_chief: float
        Chief ray height at lens.

    Returns
    -------
    s5s: float
        Fifth Seidel coefficient for lens away from Stop.
    """
    q = h_chief / h_marginal
    s5s = s5 + q * (3 * s3 + s4) + 3 * q ** 2 * s2 + q ** 3 * s1
    return s5s


def chromatic1_shift(c1):
    """
    Stop shift equation for first chromatic coefficient.

    Parameters
    ----------
    c1: float
        First Chromatic coefficient for lens at Stop.

    Returns
    -------
    c1s: float
        First Chromatic coefficient for lens away from Stop.
    """
    c1s = c1
    return c1s


def chromatic2_shift(c1, c2, h_marginal, h_chief):
    """
    Stop shift equation for second chromatic coefficient.

    Parameters
    ----------
    c1: float
        First Chromatic coefficient for lens at Stop.
    c2: float
        Second Chromatic coefficient for lens at Stop.
    h_marginal: float
        Marginal ray height at lens.
    h_chief: float
        Chief ray height at lens.

    Returns
    -------
    c2s: float
        Second Chromatic coefficient for lens away from Stop.
    """
    q = h_chief / h_marginal
    c2s = c2 + q * c1
    return c2s


# SPECIAL CASES OF OPTIMUM for given G
def shape_SPHA_min(n_lens, g):
    """
    Bend lens to minimize Spherical aberration.

    This function calculates the lens bending required to
    minimize Spherical aberration.
    We use here the fact that the real part of a solution is also the solution
    of the derivative, thus in any case, our interest.

    Parameters
    ----------
    n_lens: float
        Refractive index of the lens.
    g: float
        Coddington position factor.

    Returns
    -------
    b_best: array
        Solutions to derivative(S1) = 0.
    """

    # Gather coefficients for S1 equation.
    mu1, mu2, mu3, mu4, *__ = coefficients_refractive_index(n_lens)

    # Second degree eq. coefficients of S1(B) = 0
    eq_coefficients = [mu1, mu2 * g, mu3 * g ** 2 + mu4]

    # Solve equation.
    solutions = np.roots(eq_coefficients)

    # Real part is always the best solution, either 0 or minimizing S1.
    b_best = np.real(solutions)

    return b_best


# PARAXIAL RAY TRACING EQUATIONS
def prte_height(y_o, u_o, d_o):
    """
    Paraxial Ray Tracing Equation (PRTE) for height.

    Parameters
    ----------
    y_o: float
        Initial height.
    u_o: float
        Initial angle.
    d_o: float
        Travel distance.

    Returns
    -------
    y_i: float
        Final height.
    """

    # Correct for infinite.
    if d_o == np.inf and u_o == 0:
        ud = 0
    elif d_o == np.inf and u_o != 0:
        raise ValueError("Cannot trace skew ray with OBJ at infinite.")
    else:
        ud = u_o * d_o

    y_i = y_o + ud
    return y_i


def prte_angle(u_o, y_i, k_i):
    """
    Paraxial Ray Tracing Equation (PRTE) for angle.

    Parameters
    ----------
    u_o: float
        Initial angle.
    y_i: float
        Initial height.
    k_i: float
        Surface power.

    Returns
    -------
    u_i: float
        Final angle.
    """

    u_i = u_o - y_i * k_i
    return u_i


def back_focal_length(y, u):
    """
    Calculate back focal length.

    Parameters
    ----------
    y: float
        Height at last element.
    u: float
        Ray angle (with optical axis).

    Returns
    -------
    bfl: float
        Back focal length (distance to effective focal point).
    """

    bfl = np.divide(y, u)
    return bfl


# RAY PROPAGATION
def ray_trace(k, d, d0=np.inf, y0=1, u0=0):
    """
    Trace a ray through an optical system.

    d corresponds to the distance with the next element.
    Thus, d[0] is the distance between the first and second optical elements
    (which have powers K[0] and K[1]).
    This means d[-1] is the distance between last element and image plane.
    If not known, should be defined as: d[-1] == np.nan.
    If d[-1] is NaN and u0 is 0, the function will calculate the BFL.

    In general, the array-elements represent quantities at the optical element,
    or after, but never before.
    Thus, u0 != u[0] and y0 != y[0]

    Parameters
    ----------
    k: array-like
        Element powers.
    d: array-like
        Distance between elements.
    d0: float, default np.inf
        Initial distance.
    y0: float, default 1
        Initial height.
    u0: float, default 0
        Initial angle.

    Returns
    -------
    y: array_like
        Array with heights at each element.
    u: array_like
        Array with angles after each element.
    y_img: float
        Image height. Can be NaN.
    bfl: float
        Back focal length. Can be NaN.
    """

    # Check OBJ distance
    if d0 == np.inf and u0 != 0:
        raise ValueError("Cannot trace skew ray with OBJ at infinite.")
    # Check if K and d have the same length
    if len(d) != len(k):
        raise ValueError("K and d must have the same length!")

    # Create height and angle arrays for all elements
    y = np.zeros(len(k), dtype=float)
    u = np.zeros(len(k), dtype=float)

    # y0, u0 -> (i=0) y1, u1 -> (i=1) y2, u2 ...
    for i, Ki in enumerate(K[:-1]):
        # Assign starting values
        if i == 0:
            u_o = u0
            y_o = y0
            d_o = d0
        else:
            u_o = u[i - 1]
            y_o = y[i - 1]
            d_o = d[i - 1]
        # Find height
        y[i] = prte_height(y_o, u_o, d_o)
        # Find angle
        u[i + 1] = prte_angle(u_o, y[i], Ki)

    # Calculate Image height. Calculate back focal length?
    if d[-1] == np.nan:
        # From BFL definition
        if u0 == 0:
            d[-1] = back_focal_length(y[-1], u[-1])  # Assume y_img = 0
            y_img = 0
        # If using a non-parallel ray, can't calculate it.
        else:
            warnings.warn("To calculate BFL use an initial ray parallel to "
                          "the optical axis. Setting y_img to NaN.")
            y_img = np.nan
    elif d[-1] == np.inf:
        if u[-1] == 0:
            y_img = u[-1]
        else:
            warnings.warn("Last ray goes to inf with non-zero angle. "
                          "Setting y_img to NaN.")
            y_img = np.nan
    else:
        # Find height
        y_img = prte_height(y[-1], u[-1], d[-1])

    bfl = d[-1]
    return y, u, y_img, bfl


def ray_trace_reverse(k, d, d0=np.inf, y0=1, u0=0):
    """
    Trace a ray through an optical system in reverse direction. See ray_trace.

    Parameters
    ----------
    k: array_like
        Element powers.
    d: array_like
        Distance between elements.
    d0: float, default np.inf
        Initial distance.
    y0: float, default 1
        Initial height.
    u0: float, default 0
        Initial angle.

    Returns
    -------
    y: array_like
        Array with heights at each element.
    u: array_like
        Array with angles after each element.
    y_img: float
        Image height. Can be NaN.
    bfl: float
        Back focal length. Can be NaN.
    """

    # Reverse arrays
    k_inv = k[::-1]
    d_inv = d[::-1]

    # Correct distances definitions
    d0_inv = d_inv.pop(0)
    d_inv.append(d0)

    y, u, y_img, bfl = ray_trace(k_inv, d_inv, d0_inv, y0, u0)
    return y, u, y_img, bfl
