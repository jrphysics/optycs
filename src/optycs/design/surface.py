"""
Set of tools to help with surface pre-design.

Includes:
    |  Surface Seidel coefficients.
    |  Refractive surface equations.
    |  Reflective surface (mirror) equations.
"""

from optycs.design import thin_lens


# Surface Seidel coefficients
def seidel1(r, h, u0, n0, n1):
    """
    First Seidel coefficient for surface.

    Parameters
    ----------
    r: float
        Surface radius.
    h: float
        Ray (marginal) height at surface.
    u0: float
        Ray (marginal) input angle.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    s1: float
        First Seidel.
    """
    k = mirror_power(r)
    u1 = thin_lens.prte_angle(u0, h, k)
    s1 = A(r, h, u0, n0)**2 * h * delta_un(u0, u1, n0, n1)
    return s1


def seidel2(r, h, h_, u0, u0_, n0, n1):
    """
    Second Seidel coefficient for surface.

    Parameters
    ----------
    r: float
        Surface radius.
    h: float
        Marginal ray height at surface.
    h_: float
        Chief ray height at surface.
    u0: float
        Marginal ray input angle.
    u0_: float
        Chief ray input angle.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    s2: float
        Second Seidel.
    """
    k = mirror_power(r)
    u1 = thin_lens.prte_angle(u0, h, k)
    s2 = (A(r, h, u0, n0) * A(r, h_, u0_, n0) * h * delta_un(u0, u1, n0, n1))
    return s2


def seidel3(r, h, h_, u0, u0_, n0, n1):
    """
    Third Seidel coefficient for surface.

    Parameters
    ----------
    r: float
        Surface radius.
    h: float
        Marginal ray height at surface.
    h_: float
        Chief ray height at surface.
    u0: float
        Marginal ray input angle.
    u0_: float
        Chief ray input angle.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    s3: float
        Third Seidel.
    """
    k = mirror_power(r)
    u1 = thin_lens.prte_angle(u0, h, k)
    s3 = (A(r, h_, u0_, n0)**2 * h * delta_un(u0, u1, n0, n1))
    return s3


def seidel4(r, n0, n1, opt_inv):
    """
    Fourth Seidel coefficient for surface.

    Parameters
    ----------
    r: float
        Surface radius.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.
    opt_inv: float
        Optical invariant.

    Returns
    -------
    s4: float
        Fourth Seidel.
    """
    s4 = -opt_inv**2 / r * delta_1n(n0, n1)
    return s4


def seidel5(r, h, h_, u0, u0_, n0, n1, opt_inv):
    """
    Fifth Seidel coefficient for surface.

    Parameters
    ----------
    r: float
        Surface radius.
    h: float
        Marginal ray height at surface.
    h_: float
        Chief ray height at surface.
    u0: float
        Marginal ray input angle.
    u0_: float
        Chief ray input angle.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.
    opt_inv: float
        Optical invariant.

    Returns
    -------
    s5: float
        Fifth Seidel.
    """
    s3 = seidel3(r, h, h_, u0, u0_, n0, n1)
    s4 = seidel4(r, n0, n1, opt_inv)
    s5 = A(r, h_, u0_, n0) / A(r, h, u0, n0) * (s3 + s4)
    return s5


# Parameters for surface Seidels, by definition:
def A(r, h, u, n):
    """
    Useful expression.

    Parameters
    ----------
    r: float
        Surface radius.
    h: float
        Ray height at lens.
    u: float
        Ray input angle.
    n: float
        Refractive index before surface.

    Returns
    -------
    a: float
    """
    a = n * (u + h / r)
    return a


def delta_un(u0, u1, n0, n1):
    """
    Useful expression.

    Parameters
    ----------
    u0: float
        Ray input angle.
    u1: float
        Ray output angle.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    d_un: float
    """
    d_un = u1/n1 - u0/n0
    return d_un


def delta_1n(n0, n1):
    """
    Useful expression.

    Parameters
    ----------
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    d_1n: float
    """
    return 1/n1 - 1/n0
# END Parameters for surface Seidels


def focal_length(r, n0, n1):
    """
    Surface focal length.

    Parameters
    ----------
    r: float
        Surface radius.
    n0: float
        Refractive index before surface.
    n1: float
        Refractive index after surface.

    Returns
    -------
    f: float
        Surface focal length.
    """
    f = n0 / (n1-n0) * r
    return f


# Eq. for mirrors
def mirror_focal_length(r):
    """
    Mirror focal length.

    Parameters
    ----------
    r: float
        Surface radius.

    Returns
    -------
    f: float
        Mirror focal length.

    """
    f = -r/2
    return f


def mirror_power(r):
    """
    Mirror power.

    Parameters
    ----------
    r: float
        Surface radius.

    Returns
    -------
    p: float
        Mirror power.
    """
    p = -2/r
    return p
