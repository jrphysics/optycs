"""
Set of tools to help with (achromatic) doublet design.

Includes:
    |  Tools for doublet parameters.
    |  Aplanatic cemented and air-spaced doublets.
    |  Cemented and air-spaced stop-shifted doublets.
"""

import numpy as np
import sympy as sp
import warnings

from optycs.design import thin_lens


def thin_doublet_powers(k_doublet, v1, v2):
    """
    Calculate power of doublet elements based on material.

    Parameters
    ----------
    k_doublet: float
        Doublet power.
    v1, v2: float
        Abbe numbers.

    Returns
    -------
    k1, k2: float
        Powers of doublet elements.
    """

    # Front doublet
    k1 = k_doublet * v1 / (v1 - v2)
    # Back doublet
    k2 = k_doublet * v2 / (v2 - v1)

    return k1, k2


def position_factor_doublet(h_0, u_0, k1, k_total):
    """
    Calculates position factor at each element of a doublet.

    Parameters
    ----------
    h_0: float
        Incoming height.
    u_0: float
        Incoming angle.
    k1: float
        Power first element.
    k_total: float
        Total power doublet.

    Returns
    -------
    g1, g2: float
        Position factors at each element.
    """

    u_1 = u_0 - h_0 * k1
    g1 = thin_lens.position_factor_u(u_0, u_1)
    g2 = (k1 + k_total) / (k1 - k_total)

    return g1, g2


def doublet_radii(solutions, ka, kb, na, nb):
    """
    Take Ba, Bb solutions (doublet shape factors) and calculate radii.

    Parameters
    ----------
    solutions: sympy solutions (list of dicts)
        Ba, Bb solutions from sympy calculus.
    ka: float
        Power first element.
    kb: float
        Power second element.
    na: float
        Refractive index first element.
    nb: float
        Refractive index second element.

    Returns
    -------
    ra_front, ra_back, rb_front, rb_back: float
        Radii for first and second elements.
    """

    # Define Ba, Bb as sympy symbols
    ba, bb = sp.symbols('ba, bb')

    ra_front = []
    ra_back = []
    rb_front = []
    rb_back = []
    for solution in solutions:
        # Surface A
        temp1, temp2 = thin_lens.radii_from_shape(ka, solution[ba], na)

        # Since comes from sympy, convert to float.
        ra_front.append(float(temp1))
        ra_back.append(float(temp2))

        # Surface B
        temp1, temp2 = thin_lens.radii_from_shape(kb, solution[bb], nb)

        # Since comes from sympy, convert to float.
        rb_front.append(float(temp1))
        rb_back.append(float(temp2))

    return ra_front, ra_back, rb_front, rb_back


# Doublet calculations
def create_air_doublet(k_doublet, va, na, vb, nb, h, w):
    """
    Create aplanatic air-spaced doublet at Stop.

    Corrected for color (visible), SPHA, COMA.
    The elements of the doublet are called A (first / front)
    and B (second / back).
    Since it is at stop, h_chief = 0, w_marginal = 0

    Parameters
    ----------
    k_doublet: float
        Doublet power.
    va, vb: float
        Abbe numbers.
    na, nb: float
        Refractive indices (d-line).
    h: float
        Marginal ray height.
    w: float
        Chief ray angle.

    Returns
    -------
    (ra_front, ra_back): tuple
        Radii for first element.
    (rb_front, rb_back): tuple
        Radii for second element.
    """

    # Define Ba, Bb as sympy symbols
    ba, bb = sp.symbols('ba, bb')

    # 1. Calculate power of elements, this ensures achromatic condition
    ka, kb = thin_doublet_powers(k_doublet, va, vb)

    # 2. Calculate position factors.
    ga, gb = position_factor_doublet(h, 0, ka, k_doublet)

    # Optical invariant.
    opt_inv = h * w

    # 3. Calculate S1, S2 in function of Ba, Bb

    # SPHA
    s1a = thin_lens.seidel1_stop(h, ka, ba, ga, na)  # First element
    s1b = thin_lens.seidel1_stop(h, kb, bb, gb, nb)  # Second element
    s1 = s1a + s1b

    # COMA
    s2a = thin_lens.seidel2_stop(h, ka, ba, ga, opt_inv, na)  # First element
    s2b = thin_lens.seidel2_stop(h, kb, bb, gb, opt_inv, nb)  # Second element
    s2 = s2a + s2b

    # 4. Solve the system of equations S1 = 0, S2 = 0 for Ba, Bb
    sol = sp.solve((s1, s2), (ba, bb), dict=True)

    # Attention to the size of the solution!
    if np.size(sol) == 0:
        # Empty solution == no solutions.
        # In this case, derivative of S1 equal to 0, and solve again.
        # This finds the solution that minimizes S1. There is no S1 = 0.
        warnings.warn('No S1=0 solutions. Finding minimum.')
        s1_diff = sp.diff(s1)
        sol = sp.solve((s1_diff, s2), (ba, bb), dict=True)
    elif np.size(sol) > 2:
        print('Something unexpected happened!')
        print(sol)

    # 5. Calculate radii of curvature of both surfaces of both elements.
    ra_front, ra_back, rb_front, rb_back = doublet_radii(sol, ka, kb, na, nb)

    return (ra_front, ra_back), (rb_front, rb_back)


def create_air_doublet_shifted(k_doublet, va, na, vb, nb, h, w, dist_stop,
                               s1_goal=0, s2_goal=0):
    """
    Calculate air spaced doublet at a distance from the stop.

    In this configuration a doublet can be designed to compensate coma from
    another optical element. Desired values for spha and coma can be given.
    This function is in fact a generalisation of 'create_air_doublet',
    and thus also works if the distance to the stop is 0.

    Parameters
    ----------
    k_doublet: float
        Doublet power.
    va, vb: float
        Abbe numbers.
    na, nb: float
        Refractive indices (d-line).
    h: float
        Marginal ray height.
    w: float
        Chief ray angle.
    dist_stop: float
        Distance from/to system stop.
    s1_goal: float, default 0
        Desired value of Spherical aberration.
    s2_goal: float, default 0
        Desired value of Coma aberration.

    Returns
    -------
    (ra_front, ra_back): tuple
        Radii for first element.
    (rb_front, rb_back): tuple
        Radii for second element.

    """
    # Define Ba, Bb as sympy symbols
    ba, bb = sp.symbols('ba, bb')

    # 1. Calculate power of elements, this ensures achromatic condition
    ka, kb = thin_doublet_powers(k_doublet, va, vb)

    # 2. Calculate position factors.
    ga, gb = position_factor_doublet(h, 0, ka, k_doublet)

    # Optical invariant.
    opt_inv = h * w

    # Chief ray height
    h_ = thin_lens.prte_height(0, w, dist_stop)

    # 3. Calculate S1, S2 in function of Ba, Bb
    # SPHA
    s1a = thin_lens.seidel1_stop(h, ka, ba, ga, na)  # First element
    s1b = thin_lens.seidel1_stop(h, kb, bb, gb, nb)  # Second element
    s1 = s1a + s1b

    # COMA
    s2a0 = thin_lens.seidel2_stop(h, ka, ba, ga, opt_inv, na)  # First element
    s2b0 = thin_lens.seidel2_stop(h, kb, bb, gb, opt_inv, nb)  # Second element
    # Stop shift
    s2as = thin_lens.seidel2_shift(s1a, s2a0, h, h_)  # First element
    s2bs = thin_lens.seidel2_shift(s1b, s2b0, h, h_)  # Second element
    s2 = s2as + s2bs

    # 4. Solve the system of equations S1* = s1_goal, S2 = s2_goal for Ba, Bb
    sol = sp.solve((s1 - s1_goal, s2 - s2_goal), (ba, bb), dict=True)

    ra_front, ra_back, rb_front, rb_back = doublet_radii(sol, ka, kb, na, nb)

    return (ra_front, ra_back), (rb_front, rb_back)


def create_cemented_doublet(k_doublet, va, na, vb, nb, h, w):
    """
    Create aplanatic cemented doublet at Stop.

    Corrected for color (visible), SPHA, COMA.
    The elements of the doublet are called A (first / front)
    and B (second / back).
    Since it is at stop, h_chief = 0, w_marginal = 0
    Since cemented, ra_back == rb_front, or something went wrong!

    Parameters
    ----------
    k_doublet: float
        Doublet power.
    va, vb: float
        Abbe numbers.
    na, nb: float
        Refractive indices (d-line).
    h: float
        Marginal ray height.
    w: float
        Chief ray angle.

    Returns
    -------
    (ra_front, ra_back): tuple
        Radii for first element.
    (rb_front, rb_back): tuple
        Radii for second element.
    """

    # Define Ba, Bb as sympy symbols
    ba, bb = sp.symbols('ba, bb')

    # 1. Calculate power of elements, this ensures achromatic condition
    ka, kb = thin_doublet_powers(k_doublet, va, vb)

    # 2. Calculate position factors, assume object at infinite:
    ga, gb = position_factor_doublet(h, 0, ka, k_doublet)

    # And invariant!
    opt_inv = h * w

    # 3. From cemented condition and Coma free condition.
    #    Cemented condition:  c_a2 = c_b1
    #    Coma free condition: S2 = S2_a + S2_b = 0
    #    Assuming thin elements, ie. h_a = h_b == h
    eq_cemented = (ba - 1) * va / (na - 1) + (bb + 1) * vb / (nb - 1)

    # COMA
    s2a = thin_lens.seidel2_stop(h, ka, ba, ga, opt_inv, na)
    s2b = thin_lens.seidel2_stop(h, kb, bb, gb, opt_inv, nb)
    s2 = s2a + s2b

    # 4. Solve the system of equations S1=0, S2=0 for Ba, Bb
    sol = sp.solve((eq_cemented, s2), (ba, bb), dict=True)

    # Shape solutions
    # 5. Now just calculate radii of curvature of both surfaces of elements
    ra_front, ra_back, rb_front, rb_back = doublet_radii(sol, ka, kb, na, nb)

    for rab, rbf in zip(ra_back, rb_front):
        if not np.isclose(rab, rbf):
            warnings.warn('Inner radii are different, something is wrong!?')
            print('ra_back  = {:+.4f}'.format(rab))
            print('rb_front = {:+.4f}'.format(rbf))
            print()

    return (ra_front, ra_back), (rb_front, rb_back)


def create_cemented_doublet_shifted(k_doublet, va, na, vb, nb, h, w,
                                    dist_stop, s2_goal=0):
    """
    Calculate cemented doublet at a distance from the stop.

    In this configuration a doublet can be designed to compensate coma from
    another optical element. Desired values for spha and coma can be given.
    This function is in fact a generalisation of 'create_cemented_doublet',
    and thus also works if the distance to the stop is 0.
    No s1_goal since the cemented condition takes one degree of freedom.

    Parameters
    ----------
    k_doublet: float
        Doublet power.
    va, vb: float
        Abbe numbers.
    na, nb: float
        Refractive indices (d-line).
    h: float
        Marginal ray height.
    w: float
        Chief ray angle.
    dist_stop: float
        Distance from/to system stop.
    s2_goal: float, default 0
        Desired value of Coma aberration.

    Returns
    -------
    (ra_front, ra_back): tuple
        Radii for first element.
    (rb_front, rb_back): tuple
        Radii for second element.
    """

    # Define Ba, Bb as sympy symbols
    ba, bb = sp.symbols('ba, bb')

    # 1. Calculate power of elements, this ensures achromatic condition
    ka, kb = thin_doublet_powers(k_doublet, va, vb)

    # 2. Calculate position factors, assume object at infinite:
    ga, gb = position_factor_doublet(h, 0, ka, k_doublet)

    # And invariant!
    opt_inv = h * w

    # Chief ray height
    h_ = thin_lens.prte_height(0, w, dist_stop)

    # 3. From cemented condition and Coma free condition.
    #    Cemented condition:  c_a2 = c_b1
    #    Coma free condition: S2 = S2_a + S2_b = 0
    #    Assuming thin elements, ie. h_a = h_b == h
    eq_cemented = (ba - 1) * va / (na - 1) + (bb + 1) * vb / (nb - 1)

    # SPHA
    s1a = thin_lens.seidel1_stop(h, ka, ba, ga, na)  # First element
    s1b = thin_lens.seidel1_stop(h, kb, bb, gb, nb)  # Second element

    # COMA
    s2a0 = thin_lens.seidel2_stop(h, ka, ba, ga, opt_inv, na)  # First element
    s2b0 = thin_lens.seidel2_stop(h, kb, bb, gb, opt_inv, nb)  # Second element
    # Stop shift
    s2as = thin_lens.seidel2_shift(s1a, s2a0, h, h_)  # First element
    s2bs = thin_lens.seidel2_shift(s1b, s2b0, h, h_)  # Second element
    s2 = s2as + s2bs

    # 4. Solve the system of equations S1=0, S2=0 for Ba, Bb
    sol = sp.solve((eq_cemented, s2 - s2_goal), (ba, bb), dict=True)

    # Shape solutions
    # 5. Now just calculate radii of curvature of both surfaces of elements
    ra_front, ra_back, rb_front, rb_back = doublet_radii(sol, ka, kb, na, nb)

    for rab, rbf in zip(ra_back, rb_front):
        if not np.isclose(rab, rbf):
            warnings.warn('Inner radii are different, something is wrong!?')
            print('ra_back  = {:+.4f}'.format(rab))
            print('rb_front = {:+.4f}'.format(rbf))
            print()

    return (ra_front, ra_back), (rb_front, rb_back)
