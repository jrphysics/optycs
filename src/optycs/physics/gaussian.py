"""
Set of tools to help with Gaussian optics work / analysis.

Includes:
    |  Equations for Gaussian beam parameters.
    |  Intensity and electric field profiles.

References:
    [1] RP Photonics Encyclopedia
        (https://www.rp-photonics.com/encyclopedia.html)
"""

import numpy as np
from numpy import pi, sqrt, log, exp, sin


def rayleigh_length(w0, wavelength, m2=1):
    """
    Rayleigh length:
    Distance from beam waist where mode radius increased by a factor sqrt(2).
    It is half of the confocal parameter.

    Parameters
    ----------
    w0: float
        Beam waist.
    wavelength: float
        Monochromatic wavelength.
    m2: float, default 1
        M-square parameter.

    Returns
    -------
    zr: float
        Rayleigh length
    """
    zr = pi * w0 ** 2 / (m2 * wavelength)
    return zr


def beam_size(w0, z, wavelength, m2=1):
    """
    Beam size along propagation axis.

    Parameters
    ----------
    w0: float
        Beam waist.
    z: array-like
        Position along propagation axis, with waist at z = 0.
    wavelength
        Monochromatic wavelength.
    m2: float, default 1
        M-square parameter.

    Returns
    -------
    wz: array-like
        Beam size at position z.
    """
    # Beam size
    wz = w0 * sqrt(1 + (m2 * wavelength * z / (pi * w0**2))**2)
    return wz


def radius_curvature(w0, z, wavelength, m2=1):
    """
    The radius of curvature of the wavefront at distance z from waist.

    Parameters
    ----------
    w0: float
        Beam waist.
    z: array-like
        Position along propagation axis, with waist at z = 0.
    wavelength: float
        Monochromatic wavelength.
    m2: float, default 1
        M-square parameter.

    Returns
    -------
    radius: array-like
        Radius of curvature of the wavefront at distance z from waist.
    """
    # Calculate rayleigh length
    zr = rayleigh_length(w0, wavelength, m2)
    # Radius of curvature
    if z == 0:
        radius = np.inf
    else:
        radius = z * (1 + (zr/z)**2)
    return radius


def divergence(w0, wavelength, n=1, m2=1):
    """
    Beam divergence: how fast the beam expands far from the waist (far field).
    Measured with respect to the optical axis, so it is half-angle of spread.

    Parameters
    ----------
    w0: float
        Beam waist.
    wavelength: float
        Monochromatic wavelength.
    n: float, default 1
        Refractive index.
    m2: float, default 1
        M-square parameter.

    Returns
    -------
    theta: float
        Beam divergence.
    """
    theta = m2 * wavelength / (w0 * pi * n)
    return theta


def fwhm(wz):
    """
    Full width at half maximum.

    Parameters
    ----------
    wz: array-like
        Beams size at distance z from beam waist.

    Returns
    -------
    fw: array-like
        FWHM(z)
    """
    fw = wz * sqrt(2*log(2))
    return fw


def numerical_aperture(theta, n=1):
    """
    Numerical aperture of a Gaussian beam.

    Parameters
    ----------
    theta: array-like
        Beam divergence.
    n: float, default 1
        Refractive index.

    Returns
    -------
    na: array-like
        Numerical aperture.
    """
    na = n * sin(theta)
    return na


def complex_electric_field(w0, z, r, wavelength, m2=1, e0=1, r0=0):
    """
    For a monochromatic beam propagating in z direction with wavelength λ,
    the complex electric field amplitude (phasor) is calculated as follows [1].

    Parameters
    ----------
    w0: float
        Beam waist.
    z: float
        Position along propagation axis, with waist at z = 0.
    r: array-like
        Position in plane perpendicular to propagation axis.
    wavelength: float
        Monochromatic wavelength.
    m2: float, default 1
        M-square parameter.
    e0: float, default 1
        Maximum field amplitude.
    r0: float, default 0
        Center position.

    Returns
    -------
    e_rz: array_like
        Complex electric field amplitude.
    """
    # Wavenumber
    k = 2*pi / wavelength
    # Rayleigh length
    zr = rayleigh_length(w0, wavelength, m2)
    # Beam size
    wz = beam_size(w0, z, wavelength, m2)
    # Radius of curvature
    radius_z = radius_curvature(w0, z, wavelength, m2)

    # Complex electric field amplitude
    e_rz = e0 * (w0 / wz) * exp(- (r-r0)**2 / wz**2) * exp(1j * (
            k*z -
            np.arctan(z/zr) +
            k*(r-r0)**2 / (2*radius_z)
    ))
    return e_rz


def intensity_profile(w0, z, r, wavelength, m2=1, i0=1, r0=0):
    """
    Calculates a 1D Gaussian profile.

    Input
    ------
    w0: float
        Beam waist.
    z: float
        Position along propagation axis (waist at z = 0).
    r: array-like
        Cross-section axis.
    wavelength: float
        Wavelength of (laser) light.
    m2: float
        M-square parameter.
    i0: float, default 1
        Peak intensity.
    r0: float, default 0
        Center position.

    Output
    ------
    I: array-like
        Intensity gaussian profile.
    """
    # Beam size at position z
    wz = beam_size(w0, z, wavelength, m2)
    # Intensity distribution in plane perpendicular to propagation (z)
    intensity = i0 * (w0 / wz) ** 2 * exp(-2 * ((r - r0) / wz) ** 2)
    return intensity
